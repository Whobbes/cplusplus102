/*
 * session_5.cpp
 *
 *  Created on: 11 Apr 2017
 *      Author: wallace
 */

 // on why friend functions are useful
//#include <iostream>
//using namespace std;
//
//// forward declaration
//class B;
//class A {
//    private:
//      int numA;
//    public:
//      A(): numA(12) { }
//      // friend function declaration
//      friend int add(A, B);
//};
//
//class B {
//    private:
//       int numB;
//    public:
//       B(): numB(1) { }
//       // friend function declaration
//       friend int add(A , B);
//};
//
//// Function add() is the friend function of classes A and B
//// that accesses the member variables numA and numB
//int add(A objectA, B objectB)
//{
//   return (objectA.numA + objectB.numB);
//}
//
//int main()
//{
//    A objectA;
//    B objectB;
//    cout<<"Sum: "<< add(objectA, objectB);
//    return 0;
//}

#include <string>
#include <iostream>
#include <thread>

using namespace std;

// The function we want to execute on the new thread.
void task1(string msg)
{
    cout << "task1 says: " << msg;
}

void task2(string msg)
{
    cout << "task2 says: " << msg;
}

int main()
{
    // Make thread and ask to wait for them to be ready
    thread t1(task1, "Hello");
    while(!t1.joinable())
      {
	cout << "waiting" << endl;
      }
    t1.join();
    thread t2(task2, "World");
    t2.join();
}
