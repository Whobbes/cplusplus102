////============================================================================
//// Name        : 102.cpp
//// Author      : Sylvain Viguier
//// Version     :
//// Copyright   : Open Bar
//// Description : Hello World in C++, Ansi-style
////============================================================================
//
//#include <iostream>
//#include <string>
//#include <stdio.h>
//#include <ctime>
//using namespace std;
//
//int sum(int a, int b = 20);
//void p(int);
//void static_test();
//
//int global = 4;
//
//int main()
//{
//// // C++ print style \n is newline in onw go
//// cout << "yo, I am speaking to you!\n" << endl;
////
//// // Old school C style print still works
//// bool one = false;
//// printf("one = %d\r\n", one);
////
//// // un petit typedef pour se chauffer
//// typedef int duree_heures;
//// duree_heures hier = -20;
//// printf("hier = %d\r\n", hier);
////
//// // un peu denum
//// enum color { red, green, blue } c;
//// c = blue;
//// cout << blue << endl;
////
//// // un peu de scoping
//// p(global);
//// int global = 45;
//// p(global);
//// p(::global);
////
//// // Strings not using st namespace
//// std::string str = "yo";
//// cout << str << endl;
////
//// // const hack test ,haha g++ on mac do not allow change but compile and run
//// const int rock = 8;
//// int* ptr = 0;
//// ptr = (int*)( &rock );
//// *ptr  = 10;
//// p(rock);
////
//// // static
//// for (int i = 0; i < 10; i++)
////   {
////     static_test();
////   }
////
//// // produce different results here
//// for(int i = 0; i<5;)
////   {
////     //printf("%d",i++);
////     printf("%d ",++i);
////   }
//// printf("\r\n");
////
//// // default value for functions
//// int res = 0;
//// res = sum(24, 52);
//// p(res);
//// res = sum(12);
//// p(res);
////
//// // random need to set seed first, with current time for example
//// srand((unsigned)time( NULL));
//// cout << rand() << endl;
////
//// // a bit of fun with time
//// time_t now = time(0);
//// cout << now << endl;
//// tm *ltm = localtime(&now);
//// cout << ltm->tm_hour << endl;
//
// // input
//// char name[50];
//// cout << "enter your name:";
//// cin >> name;
//// cout << "your name is: " << name << endl;
//
//// // a structure
//// typedef struct mystruct
//// {
////   int a;
////   char c;
////   int array[50];
//// } mystruct_t;
////
//// struct mystruct a;
//// mystruct_t b;
//// a.a = 24;
//// b.a = 32;
//// b.c = 'r';
//// cout << "a.a "<< a.a << " b.a " << b.a << " b.c " << b.c << endl;
////
//// p(24);
//
// // Happy end
// return 0;
//}
//
//void p(int in)
//  {
//  cout << in << endl;
//  }
//
//void static_test()
//  {
//    static int stat = 0;
//    stat++;
//    if (stat == 10)
//      {
//	cout << "reached 10 restarting." << endl;
//	stat =0;
//      }
//  }
//
//// default values
//int sum(int a, int b) {
//   int result;
//
//   result = a + b;
//
//   return (result);
//}
