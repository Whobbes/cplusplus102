///*
// * session_2.cpp
// *
// *  Created on: 5 Apr 2017
// *      Author: wallace
// */
//#include <iostream>
//#include <map>
//#include <array>
//#include <vector>
//#include <string>
//#include <algorithm>
//
//using namespace std;
//
//int main(int argc, char** argv)
//{
//  // Map to store key value bits
//  std::map <string, char> grade_list;
//  grade_list["Wallace"] = 'a';
//  grade_list["Sean"] = 'b';
//  grade_list["Wallace"] = 'c';
//  grade_list["AAA"] = 'c';
//
//  // C++ 11 style
//  //for (auto it = grade_list.cbegin(); it!= grade_list.cend(); ++it)
//  for (map<string, char>::const_iterator it = grade_list.begin(); it != grade_list.end(); ++it)
//    {
//      cout << it->first << " " << it->second << endl;
//    }
//
//  // Array
//  std::array<int, 5> myarray{{1, 2, 3, 4, 5}};
//  myarray.at(1) = 4;
//  myarray[2] = 6;
//
//  for (auto &element : myarray)
//    cout << element << " ";
//  cout << endl;
//
//  printf("myarray size is: %d\n", (int)myarray.size());
//
//  std::sort(myarray.begin(), myarray.end());
//
//  for (array<int, 5>::iterator it = myarray.begin(); it != myarray.end(); ++it)
//    cout << *it << " ";
//  cout << endl;
//
//  //Vector
//  std::vector<int> vec = { 9, 7, 5, 3, 1 };
//  cout << vec.at(3) << endl;
//
//  for (auto element : vec)
//     cout << element << " ";
//   cout << endl;
//
//   // can be resized, eh ouai
//   vec = {0,1};
//
//   for (auto element : vec)
//      cout << element << " ";
//    cout << endl;
//
//    // a bit more on strings
//    std::string mystring("hello les loulous!");
//    cout << mystring << endl;
//
//    cout << "Enter your favorite color: ";
//    string color;
//    getline(cin, color);
//    cout << "your favorite color is " + color << endl;
//
//    cout << "Enter your favorite number: ";
//    string number;
//    getline(cin, number);
//    cout << "your favorite number is " + number << endl;
//
//    // a bit of enum just to remember it ok
//    enum DAYS {mon, tue, wed, thu, fri, sat, sun};
//    DAYS weekday = mon;
//    printf("today's winning code is %d", weekday);
//
//    // enum class from c++ 11 create separate namespace
//    enum class COLOR { BLUE, RED};
//    COLOR my_house_color = COLOR::BLUE;
//
//    // cool feature
//    //const int a = 4;
//    //static_assert((a == 3), "a is different from 3, I will not compile");
//
//    // for each element run functor print_it
////    for_each(vec.cbegin(), vec.cend(), print_it);
//
//}
//
//
//
//
//
//
