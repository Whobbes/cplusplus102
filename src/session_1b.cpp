///*
// * session_1b.cpp
// *
// *  Created on: 1 Apr 2017
// *      Author: wallace
// */
//
//#include <iostream>
//
//// Mother Class
//class Shape
//{
//public:
//  int public_int;
//protected:
//  int protected_int;
//};
//
//// Child Class
//class Box
//{
//public:
//  int public_int;
//  Box(int); // constructor
//  ~Box(); // destructor
//  int getPriv(); // accessor
//private:
//  int private_int;
//};
//
//Box::Box(int a): public_int(a), private_int(a) // initialization list
//{
//  std::cout << "you created me" << std::endl;
//}
//
//Box::~Box()
//{
//std::cout << "you killed me" << std::endl;
//}
//
//int Box::getPriv()
//{
//  return private_int;
//}
//
//int main()
//{
//std::cout << "welcome to my cool program" << std::endl;
//Box a(10);
//std::cout << "box.private_int = " << a.getPriv() << std::endl;
//std::cout << "box.public_int = " << a.public_int << std::endl;
////std::cout << "box.public_int = " << a.protected_int << std::endl;
//return 0;
//}
//
//
//
//
