///*
// * session_4.cpp
// *
// *  Created on: 10 Apr 2017
// *      Author: wallace
// */
//
//#include <iostream>
//using namespace std;
//
////---------------------------------------------------
//class Mother
//{
//public:
//  int m_pub_int;
//  int a;
//  int m_get_priv_int() const; // this will be inherited
//  Mother();
//  virtual void info();
//  virtual ~Mother() // in order for destructor to call child destructor too
//  {
//    cout << "Mother : destructor" << endl;
//  }
//
//private:
//  int m_priv_int;
//};
//
//Mother::Mother()
//{
//cout << "create a mother" << endl;
//a = 0;
//this->m_priv_int = 1;
//this->m_pub_int = 2;
//}
//
//int Mother::m_get_priv_int() const
//{
// return m_priv_int;
//}
//
//void Mother::info()
//{
//  cout << "mother: info" << endl;
//}
//
////---------------------------------------------------
//class Child: public Mother // public inheritance
//{
//public:
//  int a;
//  int c_pub_int;
//  Child();
//  void info() override final; // permits to check at compile time that it does override parent function
//  ~Child()
//  {
//    cout << "Child : destructor" << endl;
//  }
//  Child operator+(const Child &c)
//  {Child a;
//  a.c_pub_int = c.c_pub_int + this->c_pub_int;
//    return a;
//  }
//};
//
//void Child::info()
//{
//cout << "Child: info" << endl;
//}
//
//Child::Child()
//{
//  cout << "create a child" << endl;
//  a = -100;
//c_pub_int = 10;
//}
//
////---------------------------------------------------
//int main()
//{
////  Child c;
////  cout << c.c_pub_int << endl;
////  cout << c.m_pub_int << endl;
////  cout << c.m_get_priv_int() << endl;
////
////  c.info();
////  c.Mother::info();
////
////  Mother *m_ptr = &c;
////  m_ptr->info(); // only ok because mother method is virtual
////
////  Mother* c = new Child;
////  delete c;
////
////  // accessing element of mother with same name
////  Child c;
////  cout << c.a << endl;
////  cout << c.Mother::a << endl;
////
////  // using pointer of base class for derived element
////  // as info() is virtual even calling as parent ptr will call child method
////  Mother* m_ptr = &c;
////  m_ptr->info();
////
////  // a bit of fun with operators
////  std::string str = "Hello, ";
////  str.operator+=("world");
////  operator<<(operator<<(std::cout, str) , '\n');
////
////  // overloading the + Operator
////  Child c,d;
////  d = c+c;
////  cout << d.c_pub_int << endl;
//
//}
