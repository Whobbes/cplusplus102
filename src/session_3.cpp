///*
// * session_3.cpp
// *
// *  Created on: 8 Apr 2017
// *      Author: wallace
// */
//#include <iostream>
//#include <stringlist.h>
//#include <fstream>
//#include <set>
//#include <exception>
//
//using namespace std;
//
//int main(int argc, char** argv)
//{
//
////  // Strings manipulations
////  std::string str("Hello World");
////  cout << str << endl;
////
////  std::string substr = str.substr(6, 2);
////  cout << substr << endl;
////
////  std::cout << std::string(str.begin() + 6, str.end()) << std::endl;
////
////  // Linked list
////  struct node
////  {
////    int data;
////    node* next;
////  };
////
////  // Setting up root node
////  node* root = new node;
////  root->data = 1;
////  root->next = 0;
////
////  // Adding a node
////  node* first = new node;
////  root->next = first;
////  first->data = 2;
////  first->next = 0;
////
////  // print the linked list out with a conductor
////  node* conductor;
////  conductor = root;
////  if ( conductor != 0 ) // check that root exists
////    {
////    while ( conductor->next != 0 ) // check that there is an element after
////      {
////      cout << conductor->data;
////      conductor = conductor->next;
////      }
////    cout << conductor->data; // prints the last element
////    }
////
////  // read and write files
////  //Creates an instance of ofstream, and opens example.txt
////  std::ofstream out ( "example.txt", ios::app );
////  // Outputs to example.txt through a_file
////  out << "\ntest \ntest2 \ntest3";
////  // Close the file stream explicitly
////  out.close();
////
////  // now we are gonna read the file and print its content
////  std::ifstream in ("example.txt");
////
////  std::string line;
////  while(std::getline(in, line))
////    cout << line << endl;
////
////  // close the stream explicitely here too
////  in.close();
////
////  // more in strings manipulations
////  std::string line = "abcd    Apple butter chicken delicious    $0.62";
////  cout << "original line: " << line << endl;
////
////  // abcd
////  string::size_type codeEnd = line.find_first_of(" \t");
////  string code = line.substr(0, codeEnd);
////  cout << "looked for tab and printed waht was before: " << code << endl;
////
////  // 0.62
////  string::size_type priceBegin = line.find_last_of('$');
////  double price = atof(line.substr(priceBegin + 1).c_str());
////  cout << "looked for last $ symbol and transformed what was after in float: " << price << endl;
////
////  // Apple butter chicken delicous
////  string::size_type descBegin = line.find_first_not_of(" \t", codeEnd);
////  string::size_type descEnd   = line.find_last_not_of(" \t", priceBegin - 1);
////  string desc = line.substr(descBegin, descEnd - descBegin + 1);
////  cout << "find first character after first tab,"
////      " find last character not being a tab before $ and print in between: "
////      << desc << endl << endl;
////
////  std::string name("David Moore");
////  cout << name << " ";
////
////  name.replace(3, 2, "e");          // Dave Moore
////  cout << name << " ";
////
////  name.insert(0, "Mr. ");           // Mr. Dave Moore
////  cout << name << " ";
////
////  name.erase(4, 5);                 // Mr. Moore
////  cout << name << endl;
////
////  for (string::size_type i = 0; i < name.length(); i++)
////  {
////      name[i] = toupper (name[i]);
////      cout << name << " ";
////  }
////  cout << endl;
////
////  string str = "a big yogurt!";
////  if (str.find (' ') != string::npos)
////  {
////      cout  << "Contains at least one space!" << endl;
////  }
////  else
////  {
////      cout  << "Does not contain any spaces!" << endl;
////  }
////
////  // read files check and read each character, check unix style end of line
////  ifstream in("example.txt");
////
//// if (!in)
////    exit(EXIT_FAILURE);
////
//// char cur;
////  while (in.get(cur))
////    {
////     if (cur == '\n')
////       cout << "line end detected" << endl;
////    }
//
//  // Sets, are basic element not really used to be honest
//    std::set<int> first;                           // empty set of ints
//
//    int myints[]= {10,20,30,40,50};
//    cout << *(myints) << endl;
//
//    std::set<int> second (myints,myints+5);        // range
//    cout<<"size of second:" << second.size()<<endl;
//    set<int>::iterator iter;
//    for(iter=second.begin(); iter!=second.end();++iter)
//        cout<<(*iter)<<endl;
//
//    std::set<int> third (second);                  // a copy of second
//    std::set<int> fourth (second.begin(), second.end());  // iterator ctor
//
//    /* a tour of the containers
//     * List --> very generic storage
//     * Vector --> contigus element, can need reallocation
//     * Array --> fixe length
//     * Deque --> double-ended queue front and back
//     * Stack --> LIFO
//     * Queue --> FIFO
//     * Priority Queue --> bigger element first out
//     * Map --> associative list, unique value, sorted by <, alphabetical by default
//     * Set --> math collection of element order do not matter, value is the key
//     * Multi-map --> Duplicate map
//     * Multi-set --> Duplicate set
//     */
//
//    // Exceptions in C++
//    try
//    {
//	throw std::invalid_argument( "received negative value" );
//    }
//    catch (std::invalid_argument &e) // for this type of exception
//    {
//	cout << e.what() << endl;
//	exit(EXIT_FAILURE);
//    }
//    catch(std::exception &e) // for generic exception
//    {
//          std::cout << "MyException caught" << std::endl;
//          std::cout << e.what() << std::endl;
//    }
//    catch(...)
//    {
//	// for all the other problems of the world
//    }
//}
//
//
//
//
//
//
//
